from location_service.Engines.search_engine import SearchEngine
from django.http import HttpResponse
from django.core.paginator import Paginator
from location_service.models import Region
from location_service.models import Zone
from location_service.models import Street
from location_service.models import Address
import json

def addresses(request):
      pageSize = 20
      pageNumber = request.GET.get('page', 1)
      addressId = request.GET.get('id', 0)
      streetId = request.GET.get('streetId', 0)
      zoneId = request.GET.get('zoneId', 0)
      regionId = request.GET.get('regionId', 0)
      search = request.GET.get('search', "")

      # Fetch all addresses
      query = Address.objects

      # Append addressId filter
      if addressId != 0:
            query = query.filter(id=addressId)
      # Append streetId filter
      if streetId != 0:
            query = query.filter(street_id=streetId)
      # Append zoneId filter
      if zoneId != 0:
            zones = Zone.objects.filter(id=zoneId)
            streets = Street.objects.filter(zone__in=zones)
            query = query.filter(street__in=streets)
      # Append regiond filter
      if regionId != 0:
            regions = Region.objects.filter(id=regionId)
            zones = Zone.objects.filter(region__in=regions)
            streets = Street.objects.filter(zone__in=zones)
            query = query.filter(street__in=streets)

      # search: String (optional) Provide a search term to search address by.
      if search != "":
            query = SearchEngine.searchAddress(query, search)

      addresses = query.all()
      # Create the paginator
      paginator = Paginator(addresses, pageSize)
      # Get page pageNumber
      page = paginator.get_page(pageNumber)
      # Map objects in page to dictionary    
      map_iterator = list(map(lambda model: model.dictionary, page.object_list))
      # Create the json string
      jsonDump = json.dumps({
            "page": pageNumber,
            "of": paginator.num_pages,
            "addresses": map_iterator
      })
      # Return the reponse 
      return HttpResponse(jsonDump, content_type='application/json')