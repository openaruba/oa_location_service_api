from django.http import HttpResponse
from django.core.paginator import Paginator
from location_service.models import Region
import json

def regions(request):
    pageNumber = request.GET.get('page', 1)
    # Fetch all region  
    regions = Region.objects.all()
    # Create the paginator
    paginator = Paginator(regions, 20)
    # Get page pageNumber
    page = paginator.get_page(pageNumber)
    # Map objects in page to dictionary    
    map_iterator = list(map(lambda model: model.dictionary, page.object_list))
    # Create the json string
    jsonDump = json.dumps({
          "page": pageNumber,
          "of": paginator.num_pages,
          "regions": map_iterator
    })
    # Return the reponse 
    return HttpResponse(jsonDump, content_type='application/json')