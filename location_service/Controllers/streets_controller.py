from django.http import HttpResponse
from django.core.paginator import Paginator
from location_service.models import Region
from location_service.models import Zone
from location_service.models import Street

import json

def streets(request):
    pageSize = 20
    pageNumber = request.GET.get('page', 1)
    search = request.GET.get('search', "")
    streetId = request.GET.get('id', 0)
    zoneId = request.GET.get('zoneId', 0)
    regionId = request.GET.get('regionId', 0)

    if pageNumber == 0:
        pageNumber = 1

    # Create query
    query = Street.objects

    # Append the search query 
    if search != "": 
        query = query.filter(name__icontains=search)
    # Append the streetId filter 
    if streetId != 0:
        query = query.filter(id=streetId)
    # Append the zoneId filter 
    if zoneId != 0:
        query = query.filter(zone_id=zoneId)
    # Append regionId filter
    
    # Filter the street by zoneId
    if regionId != 0: 
        region = Region.objects.filter(id=regionId)
        zones = Zone.objects.filter(region__in=region)
        query = query.filter(zone__in=zones)

    streets = query.all()
    # Create the paginator
    paginator = Paginator(streets, pageSize)
    # Get page pageNumber
    page = paginator.get_page(pageNumber)
    # Map objects in page to dictionary    
    map_iterator = list(map(lambda model: model.dictionary, page.object_list))
    # Create the json string
    jsonDump = json.dumps({
          "page": page.number,
          "of": paginator.num_pages,
          "streets": map_iterator
    })
    # Return the reponse 
    return HttpResponse(jsonDump, content_type='application/json')