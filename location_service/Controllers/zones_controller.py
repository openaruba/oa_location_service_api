
from django.http import HttpResponse
from location_service.models import Zone
from django.core.paginator import Paginator
import json

def zones(request):
    pageSize = 20
    pageNumber = request.GET.get('page', 1)
    search = request.GET.get('search', "")
    zoneId = request.GET.get('id', 0)
    regionId = request.GET.get('regionId', 0)

    if pageNumber == 0:
        pageNumber = 1

    # Create query
    query = Zone.objects

    # Append the search query 
    if search != "": 
        query = query.filter(name__icontains=search)
    # Append the zoneId filter 
    if zoneId != 0:
        query = query.filter(id=zoneId)
    # Append regionId filter
    if regionId != 0:
        query = query.filter(region_id=regionId)

    zones = query.all()
    # Create the paginator
    paginator = Paginator(zones, pageSize)
    # Get page pageNumber
    page = paginator.get_page(pageNumber)
    # Map objects in page to dictionary    
    map_iterator = list(map(lambda model:  model.dictionary, page.object_list))
    # Create the json string
    jsonDump = json.dumps({
          "page": page.number,
          "of": paginator.num_pages,
          "zones": map_iterator
    })
    # Return the reponse 
    return HttpResponse(jsonDump, content_type='application/json')