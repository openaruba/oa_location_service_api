class StringUtilities:
    @staticmethod
    def hasNumbers(inputString):
        return any(char.isdigit() for char in inputString)