from django.apps import AppConfig


class LocationServiceConfig(AppConfig):
    name = 'location_service'
    verbose_name = 'Open Aruba Location Service'
