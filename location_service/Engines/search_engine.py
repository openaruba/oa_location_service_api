from location_service.Utilities.StringUtilities import StringUtilities
from location_service.models import Region
from location_service.models import Zone
from location_service.models import Street
from location_service.models import Address

class SearchEngine:
    def searchAddress(query, search):
        splitted = search.split()
        numbered = []
        notNumbered = []
        for needle in splitted:
            if StringUtilities.hasNumbers(needle):
                numbered.append(needle)
            else:
                notNumbered.append(needle)

        streets = Street.objects
        for noNumber in notNumbered:
            streets = streets.filter(name__icontains=noNumber)

        query = query.filter(street__in=streets)

        for number in numbered:
            query = query.filter(number__icontains=number)

        return query
    
    