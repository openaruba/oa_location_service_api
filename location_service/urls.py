from django.urls import path

from . import views
from location_service.Controllers.regions_controller import regions
from location_service.Controllers.zones_controller import zones
from location_service.Controllers.streets_controller import streets
from location_service.Controllers.address_controller import addresses

urlpatterns = [
    path('', views.index, name='index'),
    path('regions', regions),
    path('zones', zones),
    path('streets', streets),
    path('addresses', addresses)
]    
