from django.db import models
import uuid

class Region(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=64, unique=True, null=False)
    created_date = models.DateTimeField('date created')
    update_date = models.DateTimeField('date updated')

    @property
    def dictionary(self): 
        return {
          "id": str(self.id),
          "name": self.name
        }

    def __str__(self):
        return self.name

    class Meta:
        db_table = "regions"
