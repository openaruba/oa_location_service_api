from django.db import models
from location_service.Models.zone import Zone
import uuid

class Street(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=64, unique=True, null=False)
    abbreviation = models.CharField(max_length=5, unique=True, null=False)
    gac_code = models.IntegerField(unique=True, null=False)
    zone = models.ForeignKey(Zone, on_delete=models.CASCADE)
    created_date = models.DateTimeField('date created')
    update_date = models.DateTimeField('date updated')

    @property
    def dictionary(self): 
        return {
          "id": str(self.id),
          "name": self.name,
          "abbreviation": self.abbreviation,
          "gacCode": self.gac_code,
          "zone": self.zone.dictionary
          }

    class Meta:
        db_table = "streets"
