from django.db import models
from location_service.Models.region import Region
import uuid

class Zone(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=64, unique=True, null=False)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    created_date = models.DateTimeField('date created')
    update_date = models.DateTimeField('date updated')

    @property
    def dictionary(self): 
        return {
          "id": str(self.id),
          "name": self.name,
          "region": self.region.dictionary
          }

    class Meta:
        db_table = "zones"
