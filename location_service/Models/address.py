from django.db import models
from location_service.Models.location import Location
from location_service.Models.street import Street
import uuid

class Address(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    number = models.CharField(max_length=12, null=False)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    street = models.ForeignKey(Street, on_delete=models.CASCADE)
    created_date = models.DateTimeField('date created')
    update_date = models.DateTimeField('date updated')

    @property
    def dictionary(self): 
        return {
          "id": str(self.id),
          "number": self.number,
          "location": self.location.dictionary,
          "street": self.street.dictionary
          }

    class Meta:
        db_table = "address"
