from django.db import models
import uuid

class Location(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    longitude = models.DecimalField(max_length=190, null=False, max_digits=190, decimal_places=12)
    latitude = models.DecimalField(max_length=100, null=False, max_digits=100, decimal_places=12)
    created_date = models.DateTimeField('date created')

    @property
    def dictionary(self): 
        return {
          "id": str(self.id),
          "longitude": str(self.longitude),
          "latitude": str(self.latitude)
          }

    class Meta:
        db_table = "locations"
